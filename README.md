# Benchmarking Suite

The full usage documentation is at http://bmkwg.web.cern.ch/bmkwg/

Please use the following mailing list for further interaction: <mailto:benchmark-suite-wg@cern.ch>.
You can also subscribe to it [here](https://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=benchmark-suite-wg)

## Implementation

* Code repository:
  - https://gitlab.cern.ch/cloud-infrastructure/cloud-benchmark-suite
  - https://gitlab.cern.ch/cloud-infrastructure/cloud-benchmark-suite-docker

* Branches: 
   * **devel**: protected branch, shall be used to automatically check the merged development from other branches. 
     - The CI job will install the suite (with 'make') and run in a SLC6 OS (docker) with cvmfs mounted

   * **master**: protected branch. Code from **testing** shall be merged into this branch. Its sole purpose is to promote the previously released \*-_testing_ RPM into ai6-stable.

   * _other branches_: use other custom branches for development, and then merge into **devel**.

* Tags:
  - Documentation
    - **docs-test-.\***: tag a commit for the CI job building test documentation
    - **docs-.\***: tag a commit for the CI job building and publishing documentation
  - Software release
    - **release-docker-.\***: tag a commit for the CI job releasing docker images 
       - The CI jobs will 
          - build a Docker container with the full suite (based on SLC6 OS)
          - push the container to the gitlab registry, tagged with the git commit tag
          - test the container running with docker and docker-volume-cvmfs as driver
          - promote the container to a new version as reported in the tag release-docker-[version number]
    - **release-test-rpm-.\*** tag a commit for the CI jobs releasing the package-_testing_.rpm version into _ai6-testing.repo_
       -  build a scratch version of the RPM. This step serves as a mockup Koji build for validating the release requirements (_.spec_ syntax, etc.)
       - If the RPM already exists it means the user did not change the version/release number in the _.spec_ file. This will cause an error in the CI build. In the other hand, if the testing version is successfully released, an email will be sent to the author of that commit, announcing success 
    - **release-rpm-.\*** tag a commit for the CI jobs promoting the previously released \*-_testing_ RPM into ai6-stable 
       - _NOTE_: The ai6 repos can be found at http://linuxsoft.cern.ch/internal/repos/ai6.repo.
