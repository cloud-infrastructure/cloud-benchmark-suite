#
#  Copyright (c) CERN 2016
#
#  Author: Cristovao Cordeiro
# 

function dockermode_dependencies {
    export IPV4_NAMESERVERS=0
    for ip in `cat /etc/resolv.conf | grep nameserver | awk -F' ' '{print $2}'`
    do
	if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]
	then
            IPV4_NAMESERVERS=1
	fi
    done

    if [ $IPV4_NAMESERVERS == "0" ]
    then
	echo ' -
      No IPv4 nameservers detected in /etc/resolv.conf.
      Docker will run with IPv4 and therefore this script might fail. Continue anyway...
     - '
	echo 'WARN: no IPv4 nameservers!'
	cat /etc/resolv.conf
    fi

    if ! hash docker 2>/dev/null
    then
	if [ ! -a /etc/yum.repos.d/docker.repo ]
	then
            sudo tee /etc/yum.repos.d/docker.repo <<-'EOF'
[dockerrepo]
name=Docker Repository
baseurl=https://yum.dockerproject.org/repo/main/centos/$releasever/
enabled=1
gpgcheck=1
gpgkey=https://yum.dockerproject.org/gpg
EOF
	fi
	sudo yum install docker-engine -y
	sudo chkconfig docker on
	START_DOCKER=1
    fi

    if [[ $(cat /etc/sysctl.conf) != *'# Added by Cloud Benchmarking Suite'* ]] && ifconfig | grep inet6 &>/dev/null
    then
	if [ ! -a /etc/sysctl.conf.bak ]
	then
            sudo cp -f /etc/sysctl.conf /etc/sysctl.conf.bak
	fi
	sudo echo "# Added by Cloud Benchmarking Suite" >> /etc/sysctl.conf
	sudo echo "net.ipv6.conf.default.disable_ipv6 = 1" >> /etc/sysctl.conf
	sudo echo "net.ipv6.conf.all.disable_ipv6 = 1" >> /etc/sysctl.conf
	sudo echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
	sudo sysctl -p
	sudo service network restart
	START_DOCKER=1
    fi

    if [ ! -a /var/run/docker.pid ] || [ ! -z $START_DOCKER ]
    then
	sudo service docker restart
	# In CernVM the docker restart takes time in background
	timeout 30s bash -c -- 'while true; do if [[ -a /var/run/docker.sock ]]; then break; fi; done;' || echo "ERR: timed out waiting for docker to start"
    fi

    # docker might still fail if /etc/resolv.conf is pointing to an ipv6 nameserver
    # validate by checking if docker can reach the image Repository
    docker search centos ||
    if [ $? -ne 0 ]
    then
	echo "WARN: docker can reach Docker Hub. Add openDNS nameserver to resolv.conf"
	sudo cp -f /etc/resolv.conf /etc/resolv.conf.bak
	sudo echo 'nameserver 8.8.8.8
    nameserver 8.8.4.4' > /etc/resolv.conf
	docker search centos ||
	if [ $? -ne 0 ]
	then
            echo "ERR: can't run docker using IPv4. Revert changes and exit..."
            sudo mv -f /etc/resolv.conf.bak /etc/resolv.conf
            sudo mv -f /etc/sysctl.conf.bak /etc/sysctl.conf
            sudo sysctl -w net.ipv6.conf.all.disable_ipv6=0
            sudo sysctl -w net.ipv6.conf.default.disable_ipv6=0
            sudo sysctl -w net.ipv4.ip_forward=0
            sudo service network restart
            exit
	fi
    fi
    # pull general container with PIPs and for DB12
    docker pull $DOCKER_IMAGE_PROFILER
}
function base_dependencies {
  # Relies on following inherited varibles:
  #   - DOCKER_MODE
  #     -- DOCKER_IMAGE_PROFILER
  #   - OFFLINE

  if [[ ! -z $DOCKER_MODE ]]
  then
    ############################
    # running with docker
    ############################
      dockermode_dependencies
  else
    ############################
    # Non-docker approach
    ############################

    #hwinfo_dependencies
    # in the process of removing hwinfo
    # redhat-lsb-core can also disappear once it is removed from parser.py
    if ! yum list installed wget;
    then
      yum install -y wget
    fi

    if ! yum list installed redhat-lsb-core;
    then
      yum install -y redhat-lsb-core
    fi

    if ! hash pip 2>/dev/null
    then
	    wget https://bootstrap.pypa.io/2.6/get-pip.py
        python get-pip.py
        rm -f get-pip.py
    fi

    [ `python --version 2>&1 | grep -ic "Python 2.6"` -gt 0 ] && pip install wheel==0.29.0 #needed because Wheel 0.30.0 dropped Python 2.6 
    [ `pip list | grep argparse -c` -lt 1 ] && pip install argparse

    if [ -z $OFFLINE ]
    then
      # Prepare dependencies for sending results to AMQ
      [[ $(locale | grep LC_ALL=) == "LC_ALL=" ]] && export LC_ALL="en_US"


      if (! pip list | grep stomp) || (! pip list | grep SOAPpy)
      then
        pip install stomp.py
        pip install SOAPpy
      fi
    fi
  fi
}


function kv_dependencies {
  # Relies on following inherited varibles:
  #   - DOCKER_MODE
  #     -- DOCKER_IMAGE_KV
  #   - OFFLINE

  if test ! -e /dev/fuse
  then
    mknod /dev/fuse c 10 229
  fi
  chmod 0666 /dev/fuse
  if [[ ! -z $DOCKER_MODE ]]
  then
    docker pull $DOCKER_IMAGE_KV
  else
    if ! hash cvmfs_config 2>/dev/null || ! hash cvmfs_talk 2>/dev/null
    then
      yum list cvmfs || cvmfs_not_found=1
      if [[ ! -z $cvmfs_not_found ]]
      then
        [ -e /etc/yum.repos.d/cernvm.repo ] || wget http://cvmrepo.web.cern.ch/cvmrepo/yum/cernvm.repo -O /etc/yum.repos.d/cernvm.repo
        [ -e /etc/pki/rpm-gpg/RPM-GPG-KEY-CernVM ] || ( wget http://cvmrepo.web.cern.ch/cvmrepo/yum/RPM-GPG-KEY-CernVM -O /etc/pki/rpm-gpg/RPM-GPG-KEY-CernVM && rpm --import http://emisoft.web.cern.ch/emisoft/dist/EMI/3/RPM-GPG-KEY-emi )
      fi
      yum install -y cvmfs --nogpgcheck
      service autofs restart
      chkconfig autofs on

      [ -d /selinux ] && echo 0 > /selinux/enforce
    fi

    if ! `mount | grep -q atlas.cern.ch`
    then
      cvmfs_config_file="/etc/cvmfs/default.local"
      if [[ -e $cvmfs_config_file ]]
      then
        conf_content=`cat $cvmfs_config_file`
        repos=`cat $cvmfs_config_file | grep CVMFS_REPOSITORIES`
        if ([[ $repos != *"atlas.cern.ch"* ]] || [[ $repos != *"atlas"* ]]) && [[ ! -z $repos ]]
        then
          new_line=`echo $repos | sed "s/'//g" | sed 's/"//g' | sed '0,/=/s//=atlas.cern.ch,/'`
          echo '# Modified by the benchmark suite' >> $cvmfs_config_file
          sed -i.bak "s/$repos/$new_line/g" $cvmfs_config_file
        elif [[ -z $repos ]] && [[ ! -z $conf_content ]]
        then
          # There are no repos but assume the conf file is well configured, just add the repo
          echo "CVMFS_REPOSITORIES=atlas.cern.ch" >> $cvmfs_config_file
        elif [[ -z $conf_content ]]
        then
          # conf is empty, populate with default conf
          default_cvmfs_conf $cvmfs_config_file
        else
          echo "CVMFS is already properly configured...moving on"
          # everything is well configured, just move on
        fi
      else
        default_cvmfs_conf $cvmfs_config_file
      fi

      cvmfs_config setup
      service autofs restart
      cvmfs_config reload
      cvmfs_config probe
    fi
  fi
}

function default_cvmfs_conf {
  conf_file=${1:-"/etc/cvmfs/default.local"}
  cat >$conf_file <<EOF
CVMFS_REPOSITORIES=atlas.cern.ch
CVMFS_QUOTA_LIMIT=6000
CVMFS_CACHE_BASE=/scratch/cache/cvmfs2
CVMFS_MOUNT_RW=yes
CVMFS_HTTP_PROXY="http://squid.cern.ch:8060|http://ca-proxy.cern.ch:3128;DIRECT"
EOF
}

function unixbench_dependencies {
  if [[ -z $DOCKER_MODE ]]
  then
    if ! hash gcc 2>/dev/null
    then
      yum install -y gcc
    fi
  fi
}

function hs06_dependencies {
    #needed to enable some libc libraries to run hs06
    # Check O/S version
    if [ ! -f /etc/redhat-release ]; then
	echo "ERROR! O/S is not a RedHat-based Linux system"
	echo "ERROR! This script is only supported on SLC6 and CC7"
	return 1
    elif egrep -q "^Scientific Linux CERN SLC release 6" /etc/redhat-release; then
	os="slc6"
    elif egrep -q "^CentOS Linux release 7" /etc/redhat-release; then
	os="cc7"
    else
	echo "ERROR! Unknown O/S '"`more /etc/redhat-release`"'"
	echo "ERROR! This script is only supported on SLC6 and CC7"
	return 1
    fi

    if [ $os == 'slc6' ]; then
	[[ ! `yum list installed HEP_OSlibs_SL6 2&> /dev/null` ]] && yum install -y HEP_OSlibs_SL6
    else
	[[ ! `yum list installed gcc-c++ 2&> /dev/null` ]] && yum install -y gcc-c++
	[[ ! `yum list installed glibc-devel.i686 2&> /dev/null` ]] && yum install -y glibc-devel.i686 libstdc++-devel.i686  #needed to include in centos6/7 the 32 bit libc dev package
    fi
}

function spec2017_dependencies {
    echo "missing dependencies for spec2017"
}
