before_script:
  - export SPEC=$(ls *.spec)
  - export NAME=$(grep Name $SPEC | awk '{print $2}' )
  - export PKGVERSION=$(grep Version $SPEC | awk '{print $2}')
  - export PKGRELEASE=$(grep Release $SPEC | awk '{print $2}')
  - export PKGNAME=$NAME-$PKGVERSION-$PKGRELEASE
  - export LOG="build_report.txt"
  - export ANNOUNCEMENT="announce.txt"

stages:
  - test
  - build
  - check
  - deploy
  - promote
  - announce

variables:
  HEPWORKLOADS_IMAGE: $CI_REGISTRY_IMAGE/hepworkloads-base:1.0
  UTIL_IMAGE: $CI_REGISTRY_IMAGE/bmk-profiler:latest
  ATLAS_KV_IMAGE: $CI_REGISTRY_IMAGE/atlas-kv-bmk:1.0

test-dev:
    # This job installs the cern-benchmark suite using "make" 
    # in a docker container having cvmfs already mounted
    # then runs the three standard benchmarks 
    stage: test
    tags:
        - cvmfs
    image: $HEPWORKLOADS_IMAGE
    script: 
        - ls /cvmfs/atlas.cern.ch
        - yum install -y yum-plugin-ovl #because of https://github.com/CentOS/sig-cloud-instance-images/issues/15
        - make -f Makefile nocvmfs 
        - cern-benchmark --benchmarks="whetstone;DB12;kv" --freetext="test-dev version ${CI_COMMIT_SHA:0:8}"  --cloud="suite-CI" --queue_host=$QUEUE_HOST --queue_port=$QUEUE_PORT --username=$QUEUE_USERNAME --password=$QUEUE_PASSWD --topic=$QUEUE_TOPIC
        - cat /tmp/cern-benchmark_root/bmk_tmp/result_profile.json
        - tar -czf cern-benchmark-test.tgz /tmp/cern-benchmark_root/ /opt/cern-benchmark/ 
    artifacts:
        paths:
           - cern-benchmark-test.tgz
        expire_in: 1 week
        when: always
    only:
        - devel

###
### Create and test docker containers
###

# Template
.build-docker-template: &build-docker-definition 
    stage: build
    tags:
        - cci-swarm-builder
    image: gitlab-registry.cern.ch/cloud/ciadm:qa
    only:
        - /^release-containers-.*$/
    before_script:
        - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN gitlab-registry.cern.ch
    script: 
        - docker build -t $IMAGE_TO_BUILD $DOCKERFILE_PATH
        - docker push $IMAGE_TO_BUILD

.test-docker-template:  &test-docker-definition 
   # Test the built docker container of the suite running on slc6
    stage: check
    tags:
        - cci-swarm-builder
    image: gitlab-registry.cern.ch/cloud/ciadm
    only:
        - /^release-containers-.*$/
    before_script:
        - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN gitlab-registry.cern.ch
    script: 
        - docker volume create -d cvmfs atlas.cern.ch || echo "ops did not work?" 
        - docker run --name test_${CI_COMMIT_SHA:0:8} -v atlas.cern.ch:/cvmfs/atlas.cern.ch $CI_REGISTRY_IMAGE:${CI_COMMIT_SHA:0:8} cern-benchmark --benchmarks="whetstone;DB12;kv" --freetext="test-docker-cvmfs version ${CI_COMMIT_SHA:0:8}"  --cloud="suite-CI" --queue_host=$QUEUE_HOST --queue_port=$QUEUE_PORT --username=$QUEUE_USERNAME --password=$QUEUE_PASSWD --topic=$QUEUE_TOPIC || echo $? > .appstatus
    after_script:
        # removing the volume just created
        - docker volume rm atlas.cern.ch || echo "ops did not work?"
        # copying the results of the running container in the CI directory to make artifacts
        - docker cp test_${CI_COMMIT_SHA:0:8}:/tmp/cern-benchmark_root $CI_PROJECT_DIR/. || echo "ops no dir to copy??"
        # cleaning up the docker running containers, removing the just created container
        - docker ps -a
        - docker rm -f test_${CI_COMMIT_SHA:0:8} || echo "ops no docker container to remove?"
        - docker ps -a
        # making artifacts 
        - tar -cvzf cern-benchmark-test.tgz cern-benchmark_root || echo "ops no tarball?"
    artifacts:
        paths:
           - cern-benchmark-test.tgz
        expire_in: 1 week
        when: always

  
# N.B.: current gitlab-ci version cannot deal with nested varaible substitution 
# https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/issues/2007
# pass directly the name of the docker image to variable IMAGE_TO_BUILD

build-docker-benchmark:
    # Build the docker container of the suite running on slc6
    <<: *build-docker-definition    # Merge the contents of the template alias
    script: 
        - docker build -t $CI_REGISTRY_IMAGE:${CI_COMMIT_SHA:0:8} .
        - docker push $CI_REGISTRY_IMAGE:${CI_COMMIT_SHA:0:8} 

 
#this doesn't work in the cci-swarm-builder
.test-docker-benchmark:
    <<: *test-docker-definition


build-docker-benchmark-standalone:
    # Build the docker container of the suite running on slc6
    <<: *build-docker-definition    # Merge the contents of the template alias
    script: 
        - docker build -t $CI_REGISTRY_IMAGE/cloud-benchmark-suite-standalone:${CI_COMMIT_SHA:0:8} -f Dockerfile_standalone .
        - docker push $CI_REGISTRY_IMAGE/cloud-benchmark-suite-standalone:${CI_COMMIT_SHA:0:8} 


test-docker-benchmark-standalone:
   # Test the built docker container of the suite running on slc6
    <<: *test-docker-definition
    script: 
        - docker run --net=host --name test_${CI_COMMIT_SHA:0:8} $CI_REGISTRY_IMAGE/cloud-benchmark-suite-standalone:${CI_COMMIT_SHA:0:8} cern-benchmark --benchmarks="whetstone;DB12;kv" --freetext="test-docker-standalone version ${CI_COMMIT_SHA:0:8}" --cloud="suite-CI" --queue_host=$QUEUE_HOST --queue_port=$QUEUE_PORT --username=$QUEUE_USERNAME --password=$QUEUE_PASSWD --topic=$QUEUE_TOPIC


promote-docker-benchmark:
    <<: *build-docker-definition    # Merge the contents of the template alias
    stage: promote
    script: 
        - docker tag  $CI_REGISTRY_IMAGE:${CI_COMMIT_SHA:0:8} $CI_REGISTRY_IMAGE:${CI_COMMIT_TAG#release-containers-}
        - docker push $CI_REGISTRY_IMAGE:${CI_COMMIT_TAG#release-containers-}
        - docker tag  $CI_REGISTRY_IMAGE:${CI_COMMIT_SHA:0:8} $CI_REGISTRY_IMAGE:latest
        - docker push $CI_REGISTRY_IMAGE:latest

        - docker tag  $CI_REGISTRY_IMAGE/cloud-benchmark-suite-standalone:${CI_COMMIT_SHA:0:8} $CI_REGISTRY_IMAGE/cloud-benchmark-suite-standalone:${CI_COMMIT_TAG#release-containers-}
        - docker push $CI_REGISTRY_IMAGE/cloud-benchmark-suite-standalone:${CI_COMMIT_TAG#release-containers-}
        - docker tag  $CI_REGISTRY_IMAGE/cloud-benchmark-suite-standalone:${CI_COMMIT_SHA:0:8} $CI_REGISTRY_IMAGE/cloud-benchmark-suite-standalone:latest
        - docker push $CI_REGISTRY_IMAGE/cloud-benchmark-suite-standalone:latest

        - docker rmi $CI_REGISTRY_IMAGE:${CI_COMMIT_SHA:0:8} 
        - docker rmi $CI_REGISTRY_IMAGE:latest
        - docker rmi $CI_REGISTRY_IMAGE/cloud-benchmark-suite-standalone:${CI_COMMIT_SHA:0:8}
        - docker rmi $CI_REGISTRY_IMAGE/cloud-benchmark-suite-standalone:latest
        - docker images

.build-docker-base:
    variables:
         IMAGE_TO_BUILD: $CI_REGISTRY_IMAGE/hepworkloads-base:1.0  #was HEPWORKLOADS_IMAGE
         DOCKERFILE_PATH: "DockerImages/docker_image_hepworkloads_base/"
    <<: *build-docker-definition    # Merge the contents of the template alias

.build-docker-util:
    variables:
         IMAGE_TO_BUILD: $CI_REGISTRY_IMAGE/bmk-profiler:latest
         DOCKERFILE_PATH: "DockerImages/docker_image_profiler/"
    <<: *build-docker-definition    # Merge the contents of the template alias

.build-docker-atlas:
    variables:
         IMAGE_TO_BUILD: $HEPWORKLOADS_IMAGE  #was ATLAS_KV_IMAGE
         DOCKERFILE_PATH: "lib/hep-workloads/atlas/kv-bmk-v17.8.0.9/"
    <<: *build-docker-definition    # Merge the contents of the template alias




###
### Release Documentation
###

# Template
.build-gitbook-template: &build-gitbook-definition
    stage: promote
    image: gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
    only:
    - /^docs-release-.*$/
    variables:
      DESTINATION: .
    before_script:
    # Download gitbook and pull its most recent release
        - npm install gitbook-cli -g
        - gitbook fetch 2.6.9
    script:
        - rm -rf $CI_PROJECT_DIR/_book
        - gitbook build $CI_PROJECT_DIR/docs $CI_PROJECT_DIR/_book
        - yum install -y sshpass
        - SSHPASS=`echo $BMKWG | base64 -d` sshpass -v -e scp -v -oStrictHostKeyChecking=no -r $CI_PROJECT_DIR/_book/* bmkwg@lxplus.cern.ch:/$DOCS_LXPLUS_DIR/$DESTINATION


test-gitbook:
  <<: *build-gitbook-definition   # Merge the contents of the template alias
  variables:
    DESTINATION: dev
  only:
    #- devel
    - /^docs-test-.*$/

promote-gitbook:
    <<: *build-gitbook-definition   # Merge the contents of the template alias

###
### RPM build and release
###

.scratchbuild:
  image: gitlab-registry.cern.ch/cloud/ciadm:latest
  stage: build
  only:
    - /^release-test-rpm-.*$/
  script:
    - yum install -y make wget git
    - echo $SDCCLOUD | base64 -d | kinit sdccloud@CERN.CH
    - koji build ai6 --scratch --wait git+ssh://git@gitlab.cern.ch:7999/cloud-infrastructure/cloud-benchmark-suite.git#$(git describe --always)

.checkpackage:
  image: gitlab-registry.cern.ch/cloud/ciadm:latest
  stage: check
  only:
    - /^release-test-rpm-.*$/
  script:
    - echo $SDCCLOUD | base64 -d | kinit sdccloud@CERN.CH
    - if [ $(koji search -r build $PKGNAME | grep $PKGNAME) ]; then echo -e "Dear user,\nthe RPM $PKGNAME you are trying to build already exists in Koji."; exit 1; fi


.release:
  image: gitlab-registry.cern.ch/cloud/ciadm:latest
  stage: deploy
  only:
    - /^release-rpm-.*$/
  script:
    - exec 2>$LOG
    - yum install -y git mailx sendmail postfix mutt wget make
    - postfix start
    - last_author_email=`git log -1 --format=%ce`
    - if [[ "$last_author_email" == *"@"* ]]; then EMAIL_TO=$last_author_email; else EMAIL_TO=$REPORT_TO; fi
    - echo $SDCCLOUD | base64 -d | kinit sdccloud@CERN.CH
    - echo -e "Report automatically generated from GitLab CI at ${CI_BUILD_REPO}\n$(date) \n" > $LOG
    - echo -e "Current build stage = $CI_BUILD_STAGE\n" >> $LOG
    - koji build ai6 git+ssh://git@gitlab.cern.ch:7999/cloud-infrastructure/cloud-benchmark-suite.git#$(git describe --always) >> $LOG
    - cat $LOG | mutt -s "$CI_BUILD_REF_NAME - GitLab CI build $CI_BUILD_ID at $CI_PROJECT_DIR" $EMAIL_TO
    - sleep 10 # make sure email is sent
  when: on_success


.promotepkg:
  image: gitlab-registry.cern.ch/cloud/ciadm:latest
  stage: promote
  only:
    - /^release-rpm-.*$/
  script:
    - yum install -y make git
    - echo $SDCCLOUD | base64 -d | kinit sdccloud@CERN.CH
    - koji tag-build ai6-stable $(PKGNAME)-$(PKGRELEASE)


.announcement:
  image: gitlab-registry.cern.ch/cloud/ciadm:latest
  stage: announce
  only:
    - /^release-rpm-.*$/
  script:
    - yum install -y git mailx sendmail postfix mutt wget make
    - postfix start
    - echo -e "Dear Cloud Benchmark Suite user, \n" > $ANNOUNCEMENT
    - echo -e "we are pleased to inform that a new version has been released ($PKGVERSION-$PKGRELEASE) and it is currently being promoted to ai6-stable.\n" >> $ANNOUNCEMENT
    - echo -e "Yours sincerely,\nCloud Benchmark Suite Working Group\n\n" >> $ANNOUNCEMENT
    - echo -e "Please DO NOT REPLY\nReport automatically generated from GitLab CI at ${CI_BUILD_REPO}\n\n$(date)" >> $ANNOUNCEMENT
    - cat $ANNOUNCEMENT | mutt -s "Cloud Benchmark Suite new release - $PKGVERSION-$PKGRELEASE" $ANNOUNCE_TO
    - sleep 10 # make sure email is sent
  when: on_success
