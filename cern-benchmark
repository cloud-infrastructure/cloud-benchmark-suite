#!/usr/bin/env bash

#
#  Copyright (c) CERN 2016
#
#  Author: Cristovao Cordeiro
#


# Base dir for all
ROOTDIR=${BMK_ROOTDIR:-"/opt/cern-benchmark"}

# $bmks is defined in common.sh
source "$ROOTDIR"/lib/common.sh

echo "export init_tests=$(date +%s)" >> ${TIMES_SOURCE_PATH}

GOOD_BMKS=0
for b in $bmks
do
  GOOD_BMKS=$((GOOD_BMKS+1))
  if [[ $b == 'kv' ]]; then
    echo "INFO: running KV..."
    # Launch in normal mode
    run_kv "$TIMES_SOURCE_PATH" "$RUNAREA_PATH" "$ROOTDIR" || true
  elif [[ $b == "compress-7zip" ]]; then
    echo "TODO" # execute_compress-7zip
  elif [[ $b == "encode-mp3" ]]; then
    echo "TODO" # execute_encode-mp3
  elif [[ $b == "x264" ]]; then
    echo "TODO" # execute_x264
  elif [[ $b == "build-linux-kernel" ]]; then
    echo "TODO" # execute_build_linux_kernel
  elif [[ $b =~ ^(hs06_(32|64))$ ]]; then
      run_hs06 "$b" || true 
  elif [[ $b == "spec2017" ]]; then
      run_spec2017 || true 
  elif [[ $b == "hyper-benchmark" ]]; then
    echo "INFO: running hyper-benchmark..."
    export HYPER_BENCHMARK=1

    hyper_bmk_RUNAREA=$RUNAREA_PATH/hyper-benchmark

    export HYPER_1minLoad_1=$(uptime | sed 's/.*load average: //' | awk -F\, '{print $1}')

    if [ -f "/etc/machinefeatures/hs06" ]
    then
      export HYPER_MACHINEFEATURES_HS06=$(cat /etc/machinefeatures/hs06)
    elif [ -f "$MACHINEFEATURES/hs06" ]
    then
      export HYPER_MACHINEFEATURES_HS06=$(cat "$MACHINEFEATURES/hs06")
    else
      echo "INFO: machinefeatures hs06 not available"
    fi

    if [ -e "$JOBFEATURES/hs06_job" ]
    then
      export HYPER_JOBFEATURES_HS06=$(cat "$JOBFEATURES/hs06_job")
    else
      echo "INFO: jobfeatures hs06 not available"
    fi

    if [ -e "$JOBFEATURES/allocated_cpu" ]
    then
        export HYPER_JOBFEATURES_ALLOCATED_CPU=$(cat "$JOBFEATURES/allocated_cpu")
    else
      echo "INFO: jobfeatures allocated_cpu not available"
    fi

    export HYPER_DB12=$(run_DB12 "$hyper_bmk_RUNAREA/DB12")
    export HYPER_1minLoad_2=$(uptime | sed 's/.*load average: //' | awk -F\, '{print $1}')
    export HYPER_WHETS=$(run_whets "$hyper_bmk_RUNAREA/whets")
    export HYPER_1minLoad_3=$(uptime | sed 's/.*load average: //' | awk -F\, '{print $1}')
  elif [[ $b == "whetstone" ]]; then
    echo "INFO: running Whetstone..."
    export WHETS=$(run_whets)
  elif [[ $b == "DB12" ]]; then
    echo "INFO: running DB12..."
    export DB12=$(run_DB12)
  else
    echo "WARNING: benchmark $b is not implemented in this suite"  >&3 
    GOOD_BMKS=$((GOOD_BMKS-1))
  fi
done

[[ $GOOD_BMKS -gt 0 ]] && run_report

END=1
