# Benchmarking activity

Please use the following mailing list for further interaction: <mailto:benchmark-suite-wg@cern.ch>.
You can also subscribe to it [here](https://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=benchmark-suite-wg)


### Specifications
1. Allow collection of a configurable number of benchmarks
   * Compare the benchmark outcome under similar conditions  
1. Have a prompt feedback about executed benchmarks
   * In production can suggest deletion and re-provisioning of underperforming VMs
1. Generalize the setup to run the benchmark suite in any cloud
   * Ease data analysis and resource accounting
1. Allow later analysis with Ipython (or any other analysis tools)
   * Compare resources on cost-to-benefit basis
1. Mimic the usage of cloud resources for experiment workloads
   * Run representative benchmarks on VMs of the same size used by VOs
1. Probe randomly assigned slots in a cloud cluster
   * Not knowing what the neighbor is doing

## Benchmark suite architecture
![System workflow](Bmk-suite.png)

The figure shows the high level architecture of the benchmark suite. The data
storage, analysis and visualization layers are purely exemplifications, as users
can opt to build/use their own transport and storage tools.  

A configurable sequence of benchmarks is executed in a VM.

At the end of the sequence, the benchmark results are organized in a JSON message
and are sent to a transport layer (currently by default it is the CERN ActiveMQ
message broker). Applications can subscribe to the transport layer in order to
consume the messages. Users can also choose not to send the benchmark results to
AMQ, running on an offline mode (see [How it works](./HowItWorks.md)) for further details).

Taking the example shown in the above figure, a dedicated consumer inserts messages
in an Elasticsearch cluster, so that the benchmark results can be visualized and
aggregated in Kibana dashboards. Several metadata (such as VM UID, CPU architecture,
OS, Cloud name, IP address, etc.) are included in the result message in order to
enable aggregations.

Further detailed analysis can be performed extracting data from the storage layer.


## Implementation

* Code repository:
  - https://gitlab.cern.ch/cloud-infrastructure/cloud-benchmark-suite
  - https://gitlab.cern.ch/cloud-infrastructure/cloud-benchmark-suite-docker
