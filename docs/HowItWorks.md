# How it works

The Benchmark suite is a tool which aggregates several different benchmarks
in one single application.

It is built to comply with several use cases by allowing the users to specify
which benchmarks to run.

At the moment, the available benchmarks on the suite are:
 * DIRAC Benchmark
 * ATLAS Kit Validation
 * Whetstone (from the UnixBench benchmark suite)
 * Hyper-benchmark<sup>\*</sup>

It is a standalone application that expects the user to pass a list of the benchmarks to be executed (together with the other possible arguments referred in [How to run](./HowToRun.md)).


### Execution modes

#### Online vs Offline
The user has the choice, at launch time, to have the benchmark results uniquely printed in the terminal at the end of the execution. This is the _Offline_ mode and must be specified. The _Online_ mode makes the suite act like a producer, expecting
the corresponding publishing arguments in the execution call (by default AMQ
is the chosen transportation layer).
On both cases, the final structured JSON file will always be generated and
available in the execution directory (by default at _/tmp_).

#### With vs without Docker
The application can either be ran with or without using Docker. The requirements will change accordingly as one can see in [Requirements](./Requirements.md).

When running the application with Docker, due to Docker itself and the eventual need to setup CVMFS, running as a regular _user_ is not possible. On the bright side the usage of Docker decouples the benchmarks execution and dependencies resolving, which might be attractive for some use cases where running the benchmark as _root_ is not a problem.

**NOTE**: when running as a _user_, please make sure all the dependencies and requirements
have already been setup.


<sup>\*</sup> _a pre-defined sequence of measurements and fast benchmarks: </br>
**1-min Load -> read machine&job features -> DB12 -> 1-min Load -> whetstone -> 1-min Load**_


__NOTE__: _DB12 = fastBmk_ for older versions
